#include <Arduino.h>
#include <ArduinoJson.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#define ENABLE_LED 1

#if ENABLE_LED
#include <FastLED.h>
#endif

#include <LedConfiguration.h>
#include <ShowMode.h>
#include "credentials.h"
#include "websocket.h"
#include "debug_serial.h"

#define LED_PIN_1 1

#define NUM_LEDS 90

#define FAST_LED  \
  if (ENABLE_LED) \
  FastLED

const char nBeings = 1;

bool connecting = false;
bool connected = false;
bool wasConnected = false;

DynamicJsonDocument doc(256);

const char nModes = 4;
const char nLetters = 2;
const int letters[nLetters] = {29, 30};
const LedConfiguration ledConfig{
    NUM_LEDS,
    new CRGB[NUM_LEDS],
    letters,
    nLetters};

struct ModeGetter
{
  String name;
  ShowMode *(*get)();
};

const ModeGetter modeGetters[nModes] = {
    ModeGetter{
        "single",
        []() -> ShowMode * {
          const char *color = doc["color"];
          return new SingleMode(color, &ledConfig);
        }},
    ModeGetter{
        "bicolor",
        []() -> ShowMode * {
          const char *color1 = doc["color1"];
          const char *color2 = doc["color2"];
          return new BicolorMode(color1, color2, &ledConfig);
        }},
    ModeGetter{
        "rainbow",
        []() -> ShowMode * {
          int length = doc["length"];
          return new RainbowMode(length, &ledConfig);
        }},
    ModeGetter{
        "scatter",
        []() -> ShowMode * {
          return new ScatterMode(&ledConfig);
        }}};

ShowMode *currentShowMode = NULL;

void connectionRoutine()
{

  if (WiFi.status() != WL_CONNECTED && connected)
  {
    connected = false;
  }

  if (!connecting && !connected)
  {
    connecting = true;
    //we start the connection process

    WiFi.begin(getSsid(), getPassword());
    DEBUG_SERIAL.println("Connection started");
    delay(33);
    return;
  }

  if (WiFi.status() == WL_CONNECT_FAILED)
  {
    connecting = false;
    connected = false;
    DEBUG_SERIAL.print(F("\nConnection to "));
    DEBUG_SERIAL.print(getSsid());
    DEBUG_SERIAL.print(F(" failed"));

    //go back to connecting
    WiFi.disconnect();
    return;
  }

  if (WiFi.status() == WL_CONNECTED && connecting)
  {
    connecting = false;
    connected = true;

    DEBUG_SERIAL.print(F("\nConnected to "));
    DEBUG_SERIAL.println(getSsid());

    return;
  }

  if (connected && !wasConnected)
  {
    wasConnected = true;
    DEBUG_SERIAL.print(F("IP: "));
    DEBUG_SERIAL.println(WiFi.localIP());
  }
}

WebSocketsClient webSocket;

boolean handleShowData(const char *serialized)
{
  DEBUG_SERIAL.println(serialized);

  deserializeJson(doc, serialized);
  const char *showMode = doc["mode"];
  DEBUG_SERIAL.println(showMode);

  for (int i = 0; i < nModes; i++)
  {
    const ModeGetter getter = modeGetters[i];
    if (getter.name == showMode)
    {
      DEBUG_SERIAL.println(getter.name);
      if (currentShowMode != NULL)
      {
        delete currentShowMode;
      }
      currentShowMode = getter.get();
      return true;
    }
  }

  return false;
}

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length)
{
  boolean res;
  switch (type)
  {
  case WStype_DISCONNECTED:
    DEBUG_SERIAL.printf("[WSc] Disconnected!\n");
    break;
  case WStype_CONNECTED:
  {
    DEBUG_SERIAL.printf("[WSc] Connected to url: %s\n", payload);

    // send message to server when Connected
    webSocket.sendTXT("Connected");
  }
  break;
  case WStype_TEXT:
    DEBUG_SERIAL.printf("[WSc] get text: %s\n", payload);
    res = handleShowData((char *)payload);
    if (!res)
    {
      webSocket.sendTXT("Invalid");
    }
    else
    {
      webSocket.sendTXT("OK");
    }
    break;
  case WStype_BIN:
    DEBUG_SERIAL.printf("[WSc] get binary length: %u\n", length);
    hexdump(payload, length);

    break;
  }
}

// void handlePostShow()
// {
//   if (server.method() != HTTP_POST)
//   {
//     server.send(405, "text/plain", "Method Not Allowed");
//   }
//   if (!server.hasArg("plain"))
//   {
//     DEBUG_SERIAL.println("received empty post");
//     server.send(400, "text/plain", "Invalid request");
//   }
//   const char *payload = server.arg("plain").c_str();

//   boolean success = handleShowData(payload);
//   if (!success)
//   {
//     server.send(400, "text/plain", "Invalid payload");
//   }
//   else
//   {
//     server.send(200, "text/plain", "OK");
//   }
// }

void setup()
{

  WiFi.mode(WIFI_STA);
  delay(1000);
  pinMode(LED_PIN_1, OUTPUT);
  DEBUG_SERIAL.begin(115200);

  delay(100);

  // server.on("/show", handlePostShow);
  DEBUG_SERIAL.println("nleds");
  DEBUG_SERIAL.println(ledConfig.nLeds);
  delay(1);

  FAST_LED.addLeds<NEOPIXEL, LED_PIN_1>(ledConfig.leds, ledConfig.nLeds);
  currentShowMode = modeGetters[3].get();

  webSocket.beginSSL("bik-bot.glitch.me", 443);
  webSocket.onEvent(webSocketEvent);
}

void loop()
{
   FastLED.setCorrection(CRGB(255, 160, 225));
  connectionRoutine();
 
  webSocket.loop();
  if (currentShowMode != NULL)
  {
    currentShowMode->loop();
  }
  set_max_power_in_milliwatts(1000);
  delay_at_max_brightness_for_power(1000 / 60);
}