
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <pgmspace.h>

WiFiServer server(80);

#define LED_PIN_1   1
#define NUM_LEDS    50

const char nBeings = 1;

boolean connecting =  false;
const char *ssid = NULL;
const char *password = NULL;
void connectionRoutine()
{

    if (WiFi.status() != WL_CONNECTED)
    {
        server.close();
        connecting = false;
    }

    if (!connecting && WiFi.status() == wl_status_t::WL_DISCONNECTED)
    {   
        connecting = true;
        //we start the connection process
       
        WiFi.begin(F("TP-Link_688C"), F("62605267"));
        Serial.println(F("Connection started"));
        delay(33);
        return;
    }
    return;

    if (WiFi.status() == wl_status_t::WL_CONNECT_FAILED)
    {
        connecting = false;
        Serial.print(F("\nConnection to "));
        Serial.print(ssid);
        Serial.print(F(" failed"));

        //go back to connecting
        WiFi.disconnect();
        return;
    }

    if (WiFi.status() == wl_status_t::WL_CONNECTED && connecting)
    {
        connecting = false;
        
        Serial.print(F("\nConnected to "));
        Serial.println(ssid);

        //Start the server
        server.begin();
        Serial.print(F("Server url: "));
        Serial.print(F("http://"));
        Serial.println(WiFi.localIP());
        return;
    }
}
void setup()
{   
    
     WiFi.mode(WIFI_STA);
    delay(1000);
    pinMode(LED_PIN_1, OUTPUT);
    Serial.begin(115200);

     delay(100);
}

void loop(){

    connectionRoutine();
    for(char i = 0; i < nBeings; i++){
    }
}