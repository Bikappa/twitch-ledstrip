#include "ShowMode.h"

SingleMode::SingleMode(const char *color, const LedConfiguration *configuration) : ShowMode(configuration)
{
    char hexColor[20];
    this->name = "single";
    sprintf(hexColor, "0x%s", color + 2);
    this->color = strtol(hexColor, NULL, 16);
}

void SingleMode::loop()
{
    for (int i = 0; i < this->configuration->nLeds; i++)
    {
        this->configuration->leds[i] = color;
    }
}

BicolorMode::BicolorMode(const char *color1, const char *color2, const LedConfiguration *configuration) : ShowMode(configuration)
{

    this->name = "bicolor";

    char hexColor1[20], hexColor2[20];
    sprintf(hexColor1, "0x%s", color1 + 2);
    sprintf(hexColor2, "0x%s", color2 + 2);
    this->color1 = strtol(hexColor1, NULL, 16);
    this->color2 = strtol(hexColor2, NULL, 16);
}

void BicolorMode::loop()
{
    int offset = 0;
    for (int letter = 0; letter < this->configuration->nSections; letter++)
    {
        const long color = letter % 2 == 0 ? this->color1 : this->color2;
        for (int i = offset; i < offset + this->configuration->sections[letter]; ++i)
        {
            this->configuration->leds[i] = color;
        }
        offset += this->configuration->sections[letter];
    }
};

RainbowMode::RainbowMode(int length, const LedConfiguration *configuration) : ShowMode(configuration)
{
    this->length = length;
}

void RainbowMode::loop()
{

    unsigned char timeOffset = (unsigned char)(((millis()) * (unsigned long)256 / (unsigned long)this->duration) % (unsigned long)256);

    for (int i = 0; i < this->configuration->nLeds; ++i)
    {
        const unsigned char hue = ((i * 256 / this->length) - timeOffset) % 256;
        CRGB color = CHSV(hue, 255, 100);
        this->configuration->leds[i] = color;
    }
};

ScatterMode::ScatterMode(const LedConfiguration *configuration) : ShowMode(configuration){
    this->initialHue = random(0, 256);

    this->previousTick = 0;
};

void ScatterMode::loop()
{
    unsigned int ledProcessed = 0;
    const unsigned char segmentMinLength = 1;
    const unsigned char segmentMaxLength = 4;

    const long tick = millis() / 1000;
    if(tick == this->previousTick){
        return;
    }
    this->previousTick = tick;
    unsigned char timeOffset = (unsigned char)(((millis()) * (unsigned long)256 / (unsigned long)this->hueRoundtripDuration));

    //hue depends only on time
    const unsigned char hue = this->initialHue +  timeOffset % 256;

    while ((int)ledProcessed < this->configuration->nLeds)
    {
        const boolean illuminated = (boolean)random(2);
        unsigned char segmentLength = (unsigned char)random(segmentMinLength, segmentMaxLength + 1);

        segmentLength = min((uint) segmentLength, this->configuration->nLeds - ledProcessed);
        for (int i = 0; i < segmentLength; i++)
        {
            this->configuration->leds[ledProcessed + i] = illuminated ? CRGB::Black : (CRGB)CHSV(hue, 255, 100);
        }

        ledProcessed += segmentLength;
    }
};