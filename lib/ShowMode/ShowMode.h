#ifndef SHOW_MODE
#define SHOW_MODE

#include <LedConfiguration.h>

class ShowMode
{
protected:
    const LedConfiguration *configuration;

public:
    ShowMode(const LedConfiguration *configuration)
    {
        this->configuration = configuration;
    }
    const char *name;
    virtual void loop(){};
};

class RainbowMode : public ShowMode
{
protected:
    int length = 25;
    unsigned long duration = 2000;

public:
    RainbowMode(int length,  const LedConfiguration *);
    void loop();
};

class SingleMode : public ShowMode
{
private:
    CRGB color = 0;

public:
    SingleMode(const char *, const LedConfiguration *);
    void loop();
};

class BicolorMode : public ShowMode
{
private:
    long color1 = 0;
    long color2 = 0;

public:
    BicolorMode(const char *, const char *, const LedConfiguration *);
    void loop();
};

class ScatterMode : public ShowMode
{
private:
    unsigned long hueRoundtripDuration = 2000;
    unsigned char initialHue = 0;
    long previousTick = 0;

public:
    ScatterMode(const LedConfiguration *);
    void loop();
};

#endif