#ifndef LED_CONFIGURATION
#define LED_CONFIGURATION

#include <FastLED.h>

struct LedConfiguration
{
    int nLeds;
    CRGB *leds;
    const int *sections;
    int nSections;
};

#endif